//
//  Client.swift
//  MainProject
//
//  Created by Illia Romanenko on 7.01.22.
//

import Foundation
import RealmSwift

class Client: Object {
    @objc dynamic var name = ""
    @objc dynamic var surname = ""
    @objc dynamic var phone = ""
    @objc dynamic var instLink = ""
    @objc dynamic var patronymic = ""

    convenience init(name: String, surname: String, phone: String, instLink: String, patronymic: String) {
        self.init()
        
        self.name = name
        self.surname = surname
        self.phone = phone
        self.instLink = instLink
        self.patronymic = patronymic
    }
}
