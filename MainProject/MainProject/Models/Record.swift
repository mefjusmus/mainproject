//
//  Record.swift
//  MainProject
//
//  Created by Illia Romanenko on 7.01.22.
//

import Foundation
import RealmSwift

class Record: Object {
    @objc dynamic var userPhone = ""
    @objc dynamic var time = ""
    @objc dynamic var service = ""

    convenience init(userPhone: String, time: String, service: String) {
        self.init()
        
        self.userPhone = userPhone
        self.time = time
        self.service = service
    }
}
