//
//  Service.swift
//  MainProject
//
//  Created by Illia Romanenko on 7.01.22.
//

import Foundation
import RealmSwift

class Service: Object {
    @objc dynamic var serviceName = ""
    @objc dynamic var price = 0.0
    @objc dynamic var duration = 0

    convenience init(serviceName: String, price: Double, duration: Int) {
        self.init()
        
        self.serviceName = serviceName
        self.price = price
        self.duration = duration
    }
}
