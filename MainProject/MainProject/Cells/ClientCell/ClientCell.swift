//
//  ClientCell.swift
//  MainProject
//
//  Created by Vladislav Suslov on 5.01.22.
//

import UIKit

class ClientCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(client: Client) {
        nameLabel.text = "\(client.name) \(client.surname)"
        phoneLabel.text = client.phone
    }

    
}
