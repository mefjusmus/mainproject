//
//  Validation.swift
//  MainProject
//
//  Created by Illia Romanenko on 7.01.22.
//

import Foundation

class Validation {
    enum ValidationTypes: String {
        case simpleText = "[A-zА-я]{2,25}"
        case email = "^[A-z0-9_.+-]+@[A-z0-9-]+(\\.[A-z0-9-]{2,})+$"
        case phone = "^(\\+375|375)(29|25|44|33)(\\d{3})(\\d{2})(\\d{2})$"
    }
    
    class func validate(string: String?, pattern: ValidationTypes) -> Bool {
        guard let string = string else { return false }
        let passPred = NSPredicate(format:"SELF MATCHES %@", pattern.rawValue)
        return passPred.evaluate(with: string)
    }
}

