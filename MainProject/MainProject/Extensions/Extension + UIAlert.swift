//
//  Extension + UIAlert.swift
//  MainProject
//
//  Created by Vladislav Suslov on 9.01.22.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(_ title: String, _ message: String,_ handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
        self.present(alert, animated: true, completion: nil)
    }
}
