//
//  Extensions + UIColor.swift
//  MainProject
//
//  Created by Vladislav Suslov on 9.01.22.
//

import Foundation
import UIKit

extension UIColor {
    open class var frameColor: UIColor {
        return UIColor(named: "frameColor")!
    }
}
