//
//  SceneDelegate.swift
//  MainProject
//
//  Created by Vladislav Suslov on 31.12.21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        window?.windowScene = windowScene
        window?.rootViewController = createTabBar()
        window?.makeKeyAndVisible()
        UINavigationBar.appearance().tintColor = .frameColor
        
    }
    
    func createTabBar() -> UITabBarController {
        let tabBarController = UITabBarController()
    
        
        let first = MainVC(nibName: String(describing: MainVC.self), bundle: nil)
        first.tabBarItem = UITabBarItem(title: "Сегодня", image: UIImage(named: "houseImage"), tag: 0)
        
        let second = ClientsVC(nibName: String(describing: ClientsVC.self), bundle: nil)
        second.tabBarItem = UITabBarItem(title: "Клиенты", image: UIImage(named: "clientsImage"), tag: 1)
        
        let third = ServicesVC(nibName: String(describing: ServicesVC.self), bundle: nil)
        third.tabBarItem = UITabBarItem(title: "Услуги", image: UIImage(named: "serviceImage"), tag: 2)
//
//        let fourth = FourthController(nibName: String(describing: FourthController.self), bundle: nil)
//        fourth.tabBarItem = UITabBarItem(title: "Fourth", image: UIImage(systemName: "bookmark"), tag: 3)
        
        let controllers: [UINavigationController] = [UINavigationController(rootViewController: first), UINavigationController(rootViewController: second), UINavigationController(rootViewController: third)]
        
        
        
        tabBarController.viewControllers = controllers
        
        tabBarController.tabBar.tintColor = .frameColor
        tabBarController.tabBar.backgroundColor = .none
        
        
        return tabBarController
    }
    
    
    func configureNavBar() {
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
            
        }
    }
    

}

