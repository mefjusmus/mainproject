//
//  MainVC.swift
//  MainProject
//
//  Created by Vladislav Suslov on 31.12.21.
//

import UIKit

class MainVC: UIViewController {
    
    @IBOutlet weak var currentDateLabel: UILabel!
    @IBOutlet var recordClientButton: UIButton!
    @IBOutlet var showSceduleButton: UIButton!
    @IBOutlet var freeTime: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentDateLabel.text = CustomDateFormatter.formatFromDate(date: Date(), format: .mainScreenDate)
        navigationController?.setNavigationBarHidden(true, animated: false)
        configureUI()
    }
    
    private func configureUI() {
        recordClientButton.layer.borderColor = UIColor.frameColor.cgColor
        showSceduleButton.layer.borderColor = UIColor.frameColor.cgColor
        freeTime.layer.borderColor = UIColor.frameColor.cgColor
    }

    @IBAction func recordClientAction(_ sender: UIButton) {
        let writeClientVC = WriteClientVC(nibName: String(describing: WriteClientVC.self), bundle: nil)
        navigationController?.pushViewController(writeClientVC, animated: true)
    }
}
