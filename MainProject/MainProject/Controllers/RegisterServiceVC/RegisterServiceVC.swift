//
//  RegisterServiceVC.swift
//  MainProject
//
//  Created by Vladislav Suslov on 9.01.22.
//

import UIKit

class RegisterServiceVC: UIViewController {
    @IBOutlet var serviceNameField: UITextField!
    @IBOutlet var durationField: UITextField!
    @IBOutlet var priceField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    private func configureUI() {
        serviceNameField.layer.borderColor = UIColor.frameColor.cgColor
        serviceNameField.borderStyle = .none
        serviceNameField.layer.sublayerTransform = CATransform3DMakeTranslation(24, 0, 0)
        durationField.layer.borderColor = UIColor.frameColor.cgColor
        durationField.borderStyle = .none
        durationField.layer.sublayerTransform = CATransform3DMakeTranslation(24, 0, 0)
        priceField.layer.borderColor = UIColor.frameColor.cgColor
        priceField.borderStyle = .none
        priceField.layer.sublayerTransform = CATransform3DMakeTranslation(24, 0, 0)
    }

    

}
