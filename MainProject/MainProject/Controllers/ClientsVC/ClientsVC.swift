//
//  ClientsVC.swift
//  MainProject
//
//  Created by Vladislav Suslov on 5.01.22.
//

import UIKit

class ClientsVC: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        
        let nib = UINib(nibName: String(describing: ClientCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: ClientCell.self))
        
       
        title = "Клиенты"
        navigationItem.setRightBarButton(UIBarButtonItem(customView: addButton.self), animated: false)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    @IBAction func addClientAction(_ sender: Any) {
        let recordVC = RecordClientVC(nibName: String(describing: RecordClientVC.self), bundle: nil)
        navigationController?.pushViewController(recordVC, animated: true)
    }
    

}

extension ClientsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ClientCell.self), for: indexPath)
        guard let clientCell = cell as? ClientCell else { return cell }
        return clientCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RealmManager.readAllClients().count
    }
    
    
}
