//
//  RecordClientVC.swift
//  MainProject
//
//  Created by Illia Romanenko on 7.01.22.
//

import UIKit

class RecordClientVC: UIViewController {

    @IBOutlet weak var surnameField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var patronymicField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    private func configureUI() {
        surnameField.layer.borderColor = UIColor.frameColor.cgColor
        surnameField.borderStyle = .none
        surnameField.layer.sublayerTransform = CATransform3DMakeTranslation(24, 0, 0)
        nameField.layer.borderColor = UIColor.frameColor.cgColor
        nameField.borderStyle = .none
        nameField.layer.sublayerTransform = CATransform3DMakeTranslation(24, 0, 0)
        patronymicField.layer.borderColor = UIColor.frameColor.cgColor
        patronymicField.borderStyle = .none
        patronymicField.layer.sublayerTransform = CATransform3DMakeTranslation(24, 0, 0)
        phoneField.layer.borderColor = UIColor.frameColor.cgColor
        phoneField.borderStyle = .none
        phoneField.layer.sublayerTransform = CATransform3DMakeTranslation(24, 0, 0)
        emailField.layer.borderColor = UIColor.frameColor.cgColor
        emailField.borderStyle = .none
        emailField.layer.sublayerTransform = CATransform3DMakeTranslation(24, 0, 0)
    }
    
    @IBAction func saveClientAction(_ sender: Any) {
        guard Validation.validate(string: surnameField.text, pattern: .simpleText),
              Validation.validate(string: nameField.text, pattern: .simpleText),
              Validation.validate(string: patronymicField.text, pattern: .simpleText),
              Validation.validate(string: phoneField.text, pattern: .phone),
              Validation.validate(string: emailField.text, pattern: .email)
        else {
            self.showAlert("Внимание", "Проверьте введенные данные", nil)
            return
        }
        
        if RealmManager.readClient(phone: phoneField.text!) == nil {
            RealmManager.saveClient(client: Client(name: nameField.text!,
                                                   surname: surnameField.text!,
                                                   phone: phoneField.text!,
                                                   instLink: "",
                                                   patronymic: patronymicField.text!))
            self.showAlert("Внимание", "Вы добавили нового клиента") { _ in
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
