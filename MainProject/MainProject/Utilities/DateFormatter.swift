//
//  DateFormatter.swift
//  MainProject
//
//  Created by Illia Romanenko on 7.01.22.
//

import Foundation


class CustomDateFormatter {
    enum DateFormats: String {
        case mainScreenDate = "EEEE, d MMMM"
    }
    
    private static let formatter = DateFormatter()
    
    class func formatFromDate(date: Date, format: DateFormats) -> String {
        formatter.dateFormat = format.rawValue
        return formatter.string(from: date) 
    }
    
}
