//
//  RealmManager.swift
//  MainProject
//
//  Created by Illia Romanenko on 7.01.22.
//

import Foundation
import RealmSwift

class RealmManager {
    static private let realm = try! Realm()
    
    class func saveClient(client: Client) {
        try? realm.write {
            realm.add(client)
        }
    }
    
    class func readClient(phone: String) -> Client? {
        return Array(realm.objects(Client.self)).filter({ $0.phone == phone }).first
    }
    
    class func readAllClients() -> [Client] {
        return Array(realm.objects(Client.self))
    }
    
//    class func saveService(service: Service) {
//        try? realm.write({
//            realm.add(service)
//        })
//    }
    
}
